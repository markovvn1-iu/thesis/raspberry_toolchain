FROM ubuntu:22.04


ARG DEFAULT_TOOLCHAIN_URL=https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.5.0-2019.12-x86_64_aarch64-linux-gnu.tar.xz


# Install basic programs
RUN set -ex \
    && apt-get -qq update \
    && apt-get -qq install wget xz-utils make cmake \
    && apt-get -qq autoclean && apt-get -qq autoremove \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


# Dowload toolchain
RUN set -ex \
    && mkdir -p /toolchain \
    && wget $DEFAULT_TOOLCHAIN_URL -O /toolchain.tar.xz \
    && tar -xJf /toolchain.tar.xz -C /toolchain \
    && rm -rf /toolchain.tar.xz \
    && mv "/toolchain/`ls /toolchain`" /toolchain/gcc-linaro

COPY . /toolchain/


# User programs
RUN set -ex \
    && apt-get -qq update \
    && apt-get -qq install mc \
    && apt-get -qq autoclean && apt-get -qq autoremove \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    # Configure workspace
    && mkdir -p /workspace
