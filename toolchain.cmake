set(TOOLCHAIN_HOST "aarch64-linux-gnu")

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

# Specify the cross compiler
set(CMAKE_C_COMPILER ${CMAKE_CURRENT_LIST_DIR}/gcc-linaro/bin/${TOOLCHAIN_HOST}-gcc)
set(CMAKE_CXX_COMPILER ${CMAKE_CURRENT_LIST_DIR}/gcc-linaro/bin/${TOOLCHAIN_HOST}-g++)

# rsync -rl --copy-unsafe-links --info=progress2 192.168.14.1:/usr .
set(CMAKE_SYSROOT ${CMAKE_CURRENT_LIST_DIR}/rootfs)
set(CMAKE_CROSSCOMPILING TRUE)

set(CMAKE_FIND_ROOT_PATH ${CMAKE_SYSROOT})

# These flags can be found by running gcc -mcpu=native -march=native -Q --help=target
set(CMAKE_C_FLAGS "-mabi=lp64 -march=armv8-a+crc -mcpu=cortex-a53 -mtune=cortex-a53 ${COMMON_FLAGS}" CACHE STRING "Flags for Raspberry PI W 2")
set(CMAKE_CXX_FLAGS "${CMAKE_C_FLAGS}" CACHE STRING "Flags for Raspberry PI W 2")


# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
